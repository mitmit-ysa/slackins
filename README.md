# Slack chrome plugin injector - hebrew support for slack

This little utility is using Electron's debug API (Using chrome's debug API) to
inject Chrome plugins into an Electron app. 
The code was developed to inject the Hebrew support (RTL) support into Slack, but the concept can be changed to other apps and plugins. 
